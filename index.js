console.log("Hello World");

function divider(sectionName) {
    console.log(`----- ${sectionName} -----`);
}
divider("Array Manipulation");

// Javascript has built-in functions and methods for arrays.
    // this allows us to manipulate and access array items.

// Mutator Methods
    /*
        - Mutator methods are functions that "mutate" or change an array after they are created.
        - These mutator methods manipulate the original array performing various tasks such as adding and removing elements.
    */

    let fruits = ['Apple', 'Orange', 'Chico', 'Lemon'];

// Push Method
    /*
        - Adds an element in the end of an array AND returns the array's length
        - Syntax
            arrayName.push('elementToAdd');
    */
divider("Push Method");

    console.log('Current Array');
    console.log(fruits);

    let fruitsLength = fruits.push('Mango');
    console.log(fruitsLength);
    console.log(fruits);

    // Without return
    /*
    function addFruit(fruit) {
        fruits.push(fruit);
        console.log(fruits);
    }

    addFruit('gum-gum fruit');
    */


    /*
    function addFruit(fruit) {
        let newFruitArray = ['Apple', 'Orange', 'Chico', 'Lemon'];
        newFruitArray.push(fruit);
        // fruits.push(fruit);
        console.log(newFruitArray);
    }

    addFruit('gum-gum fruit');

    console.log(fruits);
    */
    

    // with return
    // function addFruit(newFruit) {
    //     fruits.push(newFruit);
    //     console.log(fruits)
    //     return newFruit;
    // };

    // let addedFruit = addFruit('gum-gum fruit');
    // console.log(addedFruit);

// Pop Method
divider('Pop Method')
    /* 
        Removes the last element in an array AND returns the removed element
        Syntax:
            arrayName.pop();
    */

    let removedFruit = fruits.pop();
    console.log("pop fruit: " + removedFruit);
    console.log("Mutated array from pop method");
    console.log(fruits);

// Unshift Method
divider("Unshift Method");
    /*
        - Adds one or more elements at the beginning of an array
        Syntax:
            arrayName.unshift('elementA');
            arrayName.unshift('elementA', 'elementB');
    */

    fruits.unshift('Lime', 'Banana');
    console.log('Mutated array from unshift method: ')
    console.log(fruits);

// Shift
divider("Shift Method");
    /*
        - Removes an element at the beginning of an array AND returns the removed element;
        Syntax:
            arrayName.shift();
    */

    let anotherFruit = fruits.shift();
    console.log(anotherFruit);
    console.log("Mutated array from shift method: ")
    console.log(fruits)

// Splice
divider("Splice Method");
    /*
        - Simultaneously removes elements from a specified index number and adds element/elements
        Syntax:
            arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
    */

    fruits.splice(2, 3, 'Coconut', 'Tomatoe', 'Atis');
    console.log("Mutated array from splice method");
    console.log(fruits);

    fruits.splice(1, 2, 'Lime', 'Cherry');
    console.log("Mutated array from splice method");
    console.log(fruits);

// Sort Method
divider("Sort Method");
    /*
        - Rearranges the array elements in alphanumeric order
        Syntax:
            arrayName.sort();
    */

    fruits.sort();
    console.log('Mutated array from sort method: ');
    console.log(fruits);

//  Reverse Method
divider("Reverse Method");
    /*
        - Reverses the order of array elements
        Syntax:
            arrayName.reverse();
    */

    fruits.reverse();
    console.log('Mutated array from reverse method: ');
    console.log(fruits);

//  Non-Mutator Methods
divider("Non-mutator Methos");
    /*
        - Non-mutator methods are functions that do not modify or change an array after they're created
        - These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
    */

    let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

    //  indexOf();
        /*
            - Returns the index number of the first matching element
            - If no match was found, the result will be -1;
            - The search process will be done from first element proceeding to the last element.
            Syntax:
                arrayName.indexOf(searchValue);
                arrayName.indexOf(searchValue, fromIndex);
        */

    let firstIndex = countries.indexOf('PH');
    console.log("Result of indexOf('PH'): " + firstIndex);
    
    let indexOfSecondPH = countries.indexOf('PH', 2);
    console.log("Result of indexOf('PH'): " + indexOfSecondPH);

    let invalidCountry = countries.indexOf('BR');
    console.log("Result of indexOf('BR'): " + invalidCountry);

    // lastIndexOf();
        /*
            - Returns the index number of the last matching element found in an array;
            - The search process will be done from last element proceeding to the first element
            Syntax:
                arrayName.lastNameOf(searchValue);
                arrayName.lastIndexOf(searchValue, fromIndex);
        */

    // Getting the index number starting from the last element

    let lastIndex = countries.lastIndexOf('PH');
    console.log("The result of lastIndexOf(): " + lastIndex);

    let lastIndexStart = countries.lastIndexOf('PH', 4);
    console.log("The result of lastIndexOf(): " + lastIndexStart);

    // toString();
    let stringArray = countries.toString();
    console.log("Result from toString(): ");
    console.log(stringArray);

    // concat();
        /*
            - Combines two arrays and returns the combined result
            Syntax:
                arrayA.concat(arrayB);
                arrayA.concat(elementA);
        */


    let tasksArrayA = ['drink html', 'eat javascript'];
    let tasksArrayB = ['inhale css', 'breathes bootstrap'];
    let tasksArrayC = ['get git', 'cook node'];
    
    // Combining 2 arrays
    let tasks = tasksArrayA.concat(tasksArrayB);
    console.log(tasks);
    
    // Combining multiple arrays
    let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
    console.log(allTasks);

    // Combining arrays with elements

    let combineTasks = tasksArrayA.concat('smell express', 'have react');
    console.log(combineTasks);

    // join();
        /*
            Returns an array as a string seperated by specified seperator string
            Syntax: 
                arrayName.join('seperator');
        */

    let users = ['John', 'Jane', 'Joe', 'Robert'];
    console.log(users.join(' - '));

// Iteration methods
divider("Iteration Methods");
divider("For Each");

    // forEach()
        /*
            Syntax:
                arrayName.forEach(function(indivElement {
                    statement
                }));
        */
    // allTask = ['drink html', 'eat javascript', 'inhale css', 'breathes bootstrap', 'get git', 'cook node']

    allTasks.forEach(function(task) {
        console.log(task);
    });
    
    let filteredTasks = []
    allTasks.forEach(function(tasks) {
        
        if (tasks.length > 10) {
            filteredTasks.push(tasks);
        }
    });

    console.log("Result of filteredTasks:");
    console.log(filteredTasks);

    // map();
    divider("map()");
        /*
            - Iterates on each element AND returns new array with different values depending on the result of the function's operation
            - This is useful for performing tasks where mutating/changing the elements are required
            - Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
            Syntax:
                let/const = arrayName.map(function(indivElement))
        */
    
    let numbers = [1, 2, 3 ,4 ,5];
    let numberMap = numbers.map(function(element){
        return element * 10;
    });

    console.log(numbers);
    console.log(numberMap);

    // map() vs forEach()
        /*
            //forEach(), loops over all items in the array as does map(), but forEach() does not return a new array.
        */
    let numberForEach = numbers.forEach(function(element) {
        return element * 10;
    })

    console.log(numberForEach);

    // every() (&&)
    divider("every()");
        /*
        - Checks if all elements in an array meet the given condition
        - This is useful for validating data stored in arrays especially when dealing with large amounts of data
        - Returns a true value if all elements meet the condition and false if otherwise
        - Syntax
            let/const resultArray = arrayName.every(function(indivElement) {
                return expression/condition;
            })
    */

    // numbers = [1, 2, 3 ,4 ,5];
    let allValid = numbers.every(function(element) {
        return (element < 3);
    });
    console.log("Result of every method:");
    console.log(allValid);

    // some() (||)
    divider("some()");
    let someValid = numbers.some(function(element) {
        return (element < 2);
    });

    console.log("Result of some method:");
    console.log(someValid);

    // filter()
    divider("filter()");
        /*
        - Returns new array that contains elements which meets the given condition
        - Returns an empty array if no elements were found
        - Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods
        - Mastery of loops can help us work effectively by reducing the amount of code we use
        - Several array iteration methods may be used to perform the same result
        - Syntax
            let/const resultArray = arrayName.filter(function(indivElement) {
                return expression/condition;
            })
    */

    let filterValid = numbers.filter(function(element) {
        return (element < 3);
    })

    console.log(filterValid);

    // No elements found
    let nothingFound = numbers.filter(function(element) {
        return (element == 0);
    });

    console.log(nothingFound);

    // includes()
    divider("includes()")
        /*
        - includes() method checks if the argument passed can be found in the array.
        - it returns a boolean which can be saved in a variable.
            - returns true if the argument is found in the array.
            - returns false if it is not.
        - Syntax:
            arrayName.includes(<argumentToFind>)
        */

    let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
    let productFound1 = products.includes("Mouse");
    console.log(productFound1);

    let productFound2 = products.includes("Headset");
    console.log(productFound2);

    // reduce()
        /* 
            - Evaluates elements from left to right and returns/reduces the array into a single value
            - Syntax
                let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
                    return expression/operation
                })
            - The "accumulator" parameter in the function stores the result for every iteration of the loop
            - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
            - How the "reduce" method works
                1. The first/result element in the array is stored in the "accumulator" parameter
                2. The second/next element in the array is stored in the "currentValue" parameter
                3. An operation is performed on the two elements
                4. The loop repeats step 1-3 until all elements have been worked on
        */

    // numbers = [1, 2, 3 ,4 ,5];
    let i = 0;
    let reducedArray = numbers.reduce(function(acc, cur) {
        console.warn('current iteration '+ ++i);
        console.log('accumulator: ' + acc)
        console.log('current value: ' + cur)

        return acc + cur;
    });

    console.log("Result of reduce method: ");
    console.log(reducedArray);

    

